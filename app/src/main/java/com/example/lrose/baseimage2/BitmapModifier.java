package com.example.lrose.baseimage2;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorSpace;
import android.graphics.Paint;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;

import com.android.rssample.ScriptC_grey;
import com.android.rssample.ScriptC_rcolor;

import java.util.Random;

public class BitmapModifier {

    private static int sigma = 1;

    /**
     * Gray the pixels one to one by making a weighted average of its red, green and blue components
     * @param bmp a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public static Bitmap toGray(Bitmap bmp){
        int pixel;
        int red;
        int blue;
        int green;
        int grey;

        for(int i=0;i<bmp.getWidth();i++){
            for(int j=0;j<bmp.getHeight();j++){
                pixel=bmp.getPixel(i,j);
                red=(int)(Color.red(pixel)*0.3);
                green=(int)(Color.green(pixel)*0.59);
                blue=(int)(Color.blue(pixel)*0.11);
                grey=red+green+blue;
                pixel=Color.rgb(grey,grey,grey);
                bmp.setPixel(i,j,pixel);
            }
        }
        return bmp;
    }


    /**
     * Gray the pixels one to one by making a weighted average of its red, green and blue components
     * Better version
     * @param bmp a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public static Bitmap toGrayPixels(Bitmap bmp){
        int pixel;
        int red;
        int blue;
        int green;
        int grey;
        int width=bmp.getWidth();
        int height=bmp.getHeight();
        int size =width*height;
        int colors[] = new int[size];
        bmp.getPixels(colors,0,width,0,0,width,height);
        int pos;
        for(int i=0;i<bmp.getWidth();i++){
            for(int j=0;j<bmp.getHeight();j++){
                //pixel=bmp.getPixel(i,j);
                pos=j*width+i;
                pixel=colors[pos];
                red=(int)(Color.red(pixel)*0.3);
                green=(int)(Color.green(pixel)*0.59);
                blue=(int)(Color.blue(pixel)*0.11);
                grey=red+green+blue;
                pixel=Color.rgb(grey,grey,grey);
                colors[pos]=pixel;
            }
        }
        bmp.setPixels(colors,0,width,0,0,width,height);
        return bmp;
    }

    /**
     * Tints an image evenly with a randomly drawn hue
     * @param bmp a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public  static Bitmap  toARandomColor(Bitmap  bmp) {
        Random r = new Random();
        int color;
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int pixels[] = new int[width*height];
        bmp.getPixels(pixels,0,width,0,0,width,height);
        int pixel;
        int pos;
        float[] hsv =new float[3];
        //Tire aléatoirement une teinte de l'espace HSV
        color=r.nextInt(360);
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                //récupère chaque pixel le récupère en format HSV afin de modifier uniquement sa
                // teinte par celle tirée au hasard et récupère sa valeur en RGB afin de donner
                // cette valeur au pixel stocké.
                pos=i*width+j;
                Color.colorToHSV(pixels[pos],hsv);
                hsv[0]=color;
                pixels[pos]=Color.HSVToColor(hsv);
            }
        }
        bmp.setPixels(pixels,0,width,0,0,width,height);
        return bmp;
    }

    public static Bitmap toColorize(Bitmap bmp, int color)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);

        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());

        Random rand = new Random();
        float r = (float)(rand.nextInt(999))/1000;
        float g = (float)(rand.nextInt(999))/1000;
        float b = (float)(rand.nextInt(999))/1000;

        for(int i = 0; i < totalSize; i++)
        {
            int p = pixs[i];
            int red = Color.red(p);
            int green = Color.green(p);
            int blue = Color.blue(p);
            int alpha = Color.alpha(p);
            int newred, newblue, newgreen;


            switch(color){
                case 0:
                    newred = (int)((red * r) + (green) + (blue ));
                    newgreen = green;
                    newblue = blue;
                    break;
                case 1:
                    newred = red;
                    newgreen = green;
                    newblue = (int)((red ) + (green ) + (blue * b));
                    break;
                case 2:
                    newred = red;
                    newgreen = (int)((red) + (green * g) + (blue ));
                    newblue = blue;
                    break;
                case 3:
                    newred = (int)((red * r) + (green ) + (blue ));
                    newgreen = green;
                    newblue = (int)((red ) + (green ) + (blue * b));
                    break;
                case 4:
                    newred = (int)((red * r) + (green ) + (blue));
                    newgreen = (int)((red ) + (green * g) + (blue ));
                    newblue = blue;
                    break;
                case 5:
                    newred = red;
                    newgreen = (int)((red ) + (green * g) + (blue ));
                    newblue = (int)((red ) + (green ) + (blue * b));
                    break;
                default:
                    newred = red;
                    newgreen = green;
                    newblue = blue;
                    break;
            }

            if(newred > 255)
                newred = 255;
            if(newblue > 255)
                newblue = 255;
            if(newgreen > 255)
                newgreen = 255;
            if(newred < 0)
                newred = 0;
            if(newblue < 0)
                newblue = 0;
            if(newgreen < 0)
                newgreen = 0;
            pixs[i] = Color.argb(alpha,newred,newgreen,newblue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    /**
     * Gray all pixels except those with a very high green value
     * @param bmp a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public static Bitmap keepGreen(Bitmap bmp) {
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int pixels[] = new int[width*height];
        bmp.getPixels(pixels,0,width,0,0,width,height);
        int pos;
        int red;
        int blue;
        int green;
        int grey;
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                pos=i*width+j;
                red=(int)(Color.red(pixels[pos])*0.3);
                green=(int)(Color.green(pixels[pos])*0.59);
                blue=(int)(Color.blue(pixels[pos])*0.11);
                grey=red+green+blue;
                if(Color.green(pixels[pos])<230)
                    pixels[pos]=Color.rgb(grey,grey,grey);
            }
        }
        bmp.setPixels(pixels,0,width,0,0,width,height);
        return bmp;
    }

    public static Bitmap toKeep (Bitmap bmp, int color, int value_keep)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);
        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());

        for(int i = 0; i < totalSize; i++)
        {
            int p = pixs[i];
            int red = Color.red(p);
            int green = Color.green(p);
            int blue = Color.blue(p);
            int alpha = Color.alpha(p);

            int newred, newblue, newgreen;

            int grey = (red + green + blue) / 3;

            switch(color){
                case 0:
                    if(Color.red(pixs[i])<value_keep){
                        newred = grey;
                        newgreen = grey;
                        newblue = grey;
                    }
                    else{
                        newred = red;
                        newgreen = grey;
                        newblue = grey;
                    }
                    break;
                case 1:
                    if(Color.blue(pixs[i])<value_keep){
                        newred = grey;
                        newgreen = grey;
                        newblue = grey;
                    }
                    else {
                        newred = grey;
                        newgreen = grey;
                        newblue = blue;
                    }
                    break;
                case 2:
                    if(Color.green(pixs[i])<value_keep){
                        newred = grey;
                        newgreen = grey;
                        newblue = grey;
                    }
                    else {
                        newred = grey;
                        newgreen = green;
                        newblue = grey;
                    }
                    break;
                case 3:
                    if(Color.red(pixs[i])<value_keep || Color.blue(pixs[i])<value_keep){
                        newred = grey;
                        newgreen = grey;
                        newblue = grey;
                    }
                    else {
                        newred = red;
                        newgreen = grey;
                        newblue = blue;
                    }
                    break;
                case 4:
                    if(Color.red(pixs[i])<value_keep || Color.green(pixs[i])<value_keep){
                        newred = grey;
                        newgreen = grey;
                        newblue = grey;
                    }
                    else {
                        newred = red;
                        newgreen = green;
                        newblue = grey;
                    }
                    break;
                case 5:
                    if(Color.green(pixs[i])<value_keep || Color.blue(pixs[i])<value_keep){
                        newred = grey;
                        newgreen = grey;
                        newblue = grey;
                    }
                    else {
                        newred = grey;
                        newgreen = green;
                        newblue = blue;
                    }
                    break;
                default:
                    newred = grey;
                    newgreen = grey;
                    newblue = grey;
                    break;
            }

            if(newred > 255)
                newred = 255;
            if(newblue > 255)
                newblue = 255;
            if(newgreen > 255)
                newgreen = 255;
            if(newred < 0)
                newred = 0;
            if(newblue < 0)
                newblue = 0;
            if(newgreen < 0)
                newgreen = 0;
            pixs[i] = Color.argb(alpha,newred,newgreen,newblue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    /**
     * Increases the contrasts on a colorized image
     * Does not work
     * @param bmp a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public static Bitmap dynamicExtensionOnColor(Bitmap bmp){
        int histogramColor[][][]= new int[255][255][255];
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                int pixel=bmp.getPixel(j,i);
                histogramColor[Color.red(pixel)][Color.green(pixel)][Color.blue(pixel)]++;
            }
        }
        int histogramGrey[]= new int[255];
        for(int i=0;i<bmp.getWidth();i++){
            for(int j=0;j<bmp.getHeight();j++){
                histogramGrey[Color.red(bmp.getPixel(i,j))]++;
            }}
        return bmp;
    }

    /**
     * Increases the contrasts on a grey image
     * Does not work
     * @param bmp  a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public static Bitmap dynamicExtensionOnGray(Bitmap bmp){
        int width = bmp.getWidth();
        int height = bmp.getHeight();
        int pixels[] = new int[width*height];
        bmp.getPixels(pixels,0,width,0,0,width,height);
        float[] hsv =new float[3];
        float max=0;
        float min=1;
        float value;
        int index;
        for(int i=0;i<bmp.getWidth();i++){
            for(int j=0;j<bmp.getHeight();j++){
                int pixel=bmp.getPixel(i,j);
                Color.colorToHSV(pixel,hsv);
                value=hsv[2];
                if(value<min)
                    min=value;
                if(value>max)
                    max=value;
            }
        }
        if(min==0 && max == 1)
            return bmp;
        int lut[] = new int[256];
        for(int i=0;i<256;i++){
            lut[i]=((255*(i-(int)(min)*255))/((int)(max*255)-(int)(min*255)));
        }
        int pos;
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                pos=i*width+j;
                Color.colorToHSV(pixels[pos],hsv);
                index=((int)(255*hsv[2]));
                //index ?
                pixels[pos]=Color.HSVToColor(hsv);
            }
        }
        bmp.setPixels(pixels,0,width,0,0,width,height);
        return bmp;
    }

    /**
     * Reduce the contrasts
     * @param bmp  a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public static Bitmap dynamicReduction(Bitmap bmp){
        float[] hsv =new float[3];
        float max=0;
        float min=1;
        float value;
        int index;
        for(int i=0;i<bmp.getWidth();i++){
            for(int j=0;j<bmp.getHeight();j++){
                int pixel=bmp.getPixel(i,j);
                Color.colorToHSV(pixel,hsv);
                value=hsv[2];
                if(value<min)
                    min=value;
                if(value>max)
                    max=value;
            }
        }
        if(min==0 && max == 1)
            return bmp;
        int nmax=((int)(max*255))-10;
        int nmin=((int)(min*255))+10;
        int lut[] = new int[256];
        for(int i=0;i<256;i++){
            lut[i]=(((nmax-nmin)*(i-(int)(min*255)))/((int)(max*255)-(int)(min*255)))+nmin;
        }
        for(int i=0;i<bmp.getWidth();i++){
            for(int j=0;j<bmp.getHeight();j++){
                int pixel=bmp.getPixel(i,j);
                Color.colorToHSV(pixel,hsv);
                index=((int)(255*hsv[2]));
                hsv[2]=lut[index];
                pixel=Color.HSVToColor(hsv);
                bmp.setPixel(i,j,pixel);
            }
        }
        return bmp;
    }

    /**
     * Equalize the histogram of the image
     * @param bmp  a Bitmap on which we apply the function
     * @return the Bitmap modified
     */
    public static Bitmap histogramEqualization(Bitmap bmp) {
        int grey;
        int h[] = new int[256];
        //for(int a=0;a<256;a++){h[a]=0;}
        int c[] = new int[256];
        //for(int a=0;a<256;a++){h[a]=0;}
        int height = bmp.getHeight();
        int width = bmp.getWidth();
        int pixels[] = new int[height*width];
        bmp.getPixels(pixels,0,width,0,0,width,height);
        int pixel=0;
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                grey=Color.red(pixels[i*width+j]);
                h[grey]++;
            }
        }
        c[0]=h[0];
        for(int i=1;i<256;i++){
            c[i]=h[i]+c[i-1];
        }
        int temp;
        temp=0;
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                temp=(c[Color.red(pixels[i*width+j])]*255)/(c[255]);
                pixel=Color.rgb(temp,temp,temp);
            }
        }
        bmp.setPixels(pixels,0,width,0,0,width,height);
        return bmp;
    }
    //fonction pour favbriquer un filtre gaussien pas utiliser encore ayant mis la fonction en
    // commentaire j'ai rajouté des espaces après les @ pour qu'ils ne soient pas pris en compte
    /**
     * Calculates the discrete value at x,y of the
     * 2D gaussian distribution.
     *
     * @ param theta     the theta value for the gaussian distribution
     * @ param x         the point at which to calculate the discrete value
     * @ param y         the point at which to calculate the discrete value
     * @return          the discrete gaussian value
     */
    public static double gaussianDiscrete2D(double theta, int x, int y){
        double g = 0;
        for(double ySubPixel = y - 0.5; ySubPixel < y + 0.55; ySubPixel += 0.1){
            for(double xSubPixel = x - 0.5; xSubPixel < x + 0.55; xSubPixel += 0.1){
                g = g + ((1/(2*Math.PI*theta*theta)) *
                        Math.pow(Math.E,-(xSubPixel*xSubPixel+ySubPixel*ySubPixel)/
                                (2*theta*theta)));
            }
        }
        g = g/121;
        //System.out.println(g);
        return g;
    }

    /**
     * Calculates several discrete values of the 2D gaussian distribution.
     *
     * @param theta     the theta value for the gaussian distribution
     * @param size      the number of discrete values to calculate (pixels)
     * @return          2Darray (size*size) containing the calculated
     * discrete values
     */

    public static double [][] gaussian2D(double theta, int size){
        double [][] kernel = new double [size][size];
        for(int j=0;j<size;++j){
            for(int i=0;i<size;++i){
                kernel[i][j]=gaussianDiscrete2D(theta,i-(size/2),j-(size/2));
            }
        }

        double sum = 0;
        for(int j=0;j<size;++j){
            for(int i=0;i<size;++i){
                sum = sum + kernel[i][j];

            }
        }

        return kernel;
    }

    public  static float [][]KernelGaussian(int filterSize){
        float[][] kernel = new float[filterSize][filterSize];
        for(int y=0;y<filterSize;y++){
            for(int x=0;x<filterSize;x++){
                kernel[x][y] =(float) Math.exp(-((x*x+y*y)/(2*sigma*sigma)));
            }
        }
        return kernel;
    }
    /**
     * This function make the kernel of the type filterType and and the size filterSize
     * and use a function to use the kernel, and return the returned Bitmap
     * @param bmp a Bitmap on which we apply Convolution
     * @param filterSize the size of the filter(kernel) to apply
     * @param filterType the type of the filter(kernel) to apply
     * @return the Bitmap return by the appropriate funciton
     */
    public static Bitmap Convolution (Bitmap bmp, int filterSize, String filterType){
        if((filterSize%2)==0)
            return bmp;
        double/*int*/ filter[][];
        float temp;
        Bitmap result1;
        Bitmap result2;
        Bitmap finalSobel;
        switch(filterType){
            case "Averaging":
                filter = new double/*int*/[filterSize][filterSize];
                for(int i=0;i<filterSize;i++){
                    for(int j=0;j<filterSize;j++){
                        //temp=(1/(filterSize*filterSize));
                        temp=((float)1/((float)(filterSize*filterSize)));
                        filter[j][i]=temp;
                    }
                }
                //filter(bmp,filter,filterSize)
                result1 = filter(bmp,filter,filterSize).copy(bmp.getConfig(),bmp.isMutable());
                bmp = result1.copy(result1.getConfig(),result1.isMutable());
                //bmp = filter(bmp,filter,filterSize).copy(bmp.getConfig(),bmp.isMutable());
                break;
            case "Gaussian":
                filter = gaussian2D(3,filterSize);//KernelGaussian(filterSize);
                result1 = filter(bmp,filter,filterSize).copy(bmp.getConfig(),bmp.isMutable());
                bmp = result1.copy(result1.getConfig(),result1.isMutable());
                break;
            case "Sobel":
                assert filterSize!=3;
                if(filterSize!=3)
                    return bmp;
                //A changer peut être
                toGrayPixels(bmp);
                filter = new double/*int*/[filterSize][filterSize];
                //filter = {-1 ,0,1,-2,0,2,-1,0,1};
                filter[0][0]=-1; filter[0][1]=0; filter[0][2]=1;
                filter[1][0]=-2; filter[1][1]=0; filter[1][2]=2;
                filter[2][0]=-1; filter[2][1]=0; filter[2][2]=1;
                //filter(bmp,filter,filterSize);
                result1 = filterSobel(bmp,filter,filterSize).copy(bmp.getConfig(),bmp.isMutable());
                //filter = {-1 ,-2,-1,0,0,0,-1,-2,-1};
                filter[0][0]=-1; filter[0][1]=-2; filter[0][2]=-1;
                filter[1][0]=0; filter[1][1]=0; filter[1][2]=0;
                filter[2][0]=1; filter[2][1]=2; filter[2][2]=1;
                result2 = filterSobel(bmp,filter,filterSize).copy(bmp.getConfig(),bmp.isMutable());
                //filterSobel(bmp,filter,filterSize);
                //bmp = result1.copy(result1.getConfig(),result1.isMutable());
                //bmp = result2.copy(result2.getConfig(),result2.isMutable());
                bmp = SumSobel(result1,result2).copy(bmp.getConfig(),bmp.isMutable());
                break;
            case "Laplacian":
                if(filterSize!=3)
                    return bmp;
                filter = new /*int*/double[filterSize][filterSize];
                //filter = {1,1,1,1,-8,1,1,1,1};
                filter[0][0]=-1; filter[0][1]=-1; filter[0][2]=-1;
                filter[1][0]=-1; filter[1][1]=8; filter[1][2]=-1;
                filter[2][0]=-1; filter[2][1]=-1; filter[2][2]=-1;
                bmp = filterLaplacian(bmp,filter,filterSize).copy(bmp.getConfig(),bmp.isMutable());
                filter(bmp,filter,filterSize);
                break;

            default:
                throw new IllegalArgumentException();
        }
        return bmp;
    }

    /**
     * It's a function that I create to apply a Convolution to a Bitmap with any kernel filter
     * but I have to edit it in order to apply Sobel and Laplacian (filterSobel and filterLaplacian)
     * Convolution so there is code to factorize.
     * @param bmp a Bitmap on which we apply Convolution
     * @param filter the kernel that we use to Convolve
     * @param filterSize the size of the filter
     * @return the Bitmap modified
     */
    public static Bitmap filter (Bitmap bmp,double filter[][], int filterSize){
        //pas fait pour sobel ou laplace
        int height = bmp.getHeight();
        int width = bmp.getWidth();
        int pixels[] = new int[height*width];
        int pixelsbase[] = new int[height*width];
        float /*int*/ grey=0;
        float red=0;
        float green=0;
        float blue=0;
        float redtemp =0;
        float bluetemp =0;
        float greentemp =0;
        int ix;
        int iy;
        float   /*int*/temp=0;
        int wvalue;
        int radius=((filterSize-1)/2);
        //toGrayPixels(bmp);
        bmp.getPixels(pixels,0,width,0,0,width,height);
        bmp.getPixels(pixelsbase,0,width,0,0,width,height);
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                //temp = 0;
                for(int y=(-1*radius);y<=(radius);y++){
                    iy=Math.abs(i+y);
                    if(iy>=height)
                        iy-=(2*(iy-(height-1)));
                    for(int x=(-1*radius);x<=radius;x++){
                        ix=Math.abs(j+x);
                        if(ix>=width)
                            ix-=(2*(ix-(width-1)));

                        //grey=Color.red(pixelsbase[iy*width+ix]);
                        //wvalue =iy*width;
                        red=Color.red(pixelsbase[iy*width+ix]);
                        green=Color.green(pixelsbase[iy*width+ix]);
                        blue=Color.blue(pixelsbase[iy*width+ix]);
                        //temp+=(grey*filter[(x+(radius))][(y+(radius))]);
                        redtemp+=(red*filter[(x+radius)][(y+radius)]);
                        greentemp+=(green*filter[(x+radius)][(y+radius)]);
                        bluetemp+=(blue*filter[(x+radius)][(y+radius)]);
                    }
                }
                //Après toutes les opérations certaines valeurs peuvent prendre une valeur supérieur
                // à 255 et cela crée des artéfact j'ai donc penser a ramener ses valeurs aux maximum
                // cela semble régler le problème
                if(redtemp>255)
                    redtemp=255;
                if(bluetemp>255)
                    bluetemp=255;
                if(greentemp>255)
                    greentemp=255;
                redtemp/=255;
                greentemp/=255;
                bluetemp/=255;
                //temp/=255;
                //pixels[i*width+j]=Color.rgb(temp,temp,temp);
                pixels[i*width+j]=Color.rgb(redtemp,greentemp,bluetemp);
            }
        }
        Bitmap result = bmp.copy(bmp.getConfig(), true);
        result.setPixels(pixels,0,width,0,0,width,height);
        return result;
    }

    /**
     *
     * @param bmp a Bitmap on which we apply Convolution
     * @param filter the kernel that we use to Convolve
     * @param filterSize the size of the filter (can be taken off)
     * @return the Bitmap modified
     */
    public static Bitmap filterSobel (Bitmap bmp,double/*int*/ filter[][], int filterSize){
        //pas fait pour sobel ou laplace
        int height = bmp.getHeight();
        int width = bmp.getWidth();
        int pixels[] = new int[height*width];
        int pixelsbase[] = new int[height*width];
        float /*int*/ grey=0;
        int ix;
        int iy;
        float   /*int*/temp=0;
        int wvalue;
        int radius=((filterSize-1)/2);
        bmp.getPixels(pixels,0,width,0,0,width,height);
        bmp.getPixels(pixelsbase,0,width,0,0,width,height);
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                //temp = 0;
                for(int y=(-1*radius);y<=(radius);y++){
                    iy=Math.abs(i+y);
                    if(iy>=height)
                        iy-=(2*(iy-(height-1)));
                    for(int x=(-1*radius);x<=radius;x++){
                        ix=Math.abs(j+x);
                        if(ix>=width)
                            ix-=(2*(ix-(width-1)));

                        grey=Color.red(pixelsbase[iy*width+ix]);
                        temp+=(Math.abs(grey*filter[(x+(radius))][(y+(radius))]));

                    }
                }
                temp=Math.abs(temp);
                temp/=4;
                temp/=255;
                pixels[i*width+j]=Color.rgb(temp,temp,temp);
            }
        }
        Bitmap result = bmp.copy(bmp.getConfig(), true);
        result.setPixels(pixels,0,width,0,0,width,height);
        return result;
    }

    /**
     *
     * @param bmp a Bitmap on which we apply Convolution
     * @param filter the kernel that we use to Convolve
     * @param filterSize the size of the filter (can be taken off)
     * @return the Bitmap modified
     */
    public static Bitmap filterLaplacian (Bitmap bmp,double/*int*/ filter[][], int filterSize){
        //pas fait pour sobel ou laplace
        int height = bmp.getHeight();
        int width = bmp.getWidth();
        int pixels[] = new int[height*width];
        int pixelsbase[] = new int[height*width];
        float /*int*/ grey=0;
        int ix;
        int iy;
        float   /*int*/temp=0;
        int wvalue;
        int radius=((filterSize-1)/2);
        bmp.getPixels(pixels,0,width,0,0,width,height);
        bmp.getPixels(pixelsbase,0,width,0,0,width,height);
        for(int i=0;i<bmp.getHeight();i++){
            for(int j=0;j<bmp.getWidth();j++){
                //temp = 0;
                for(int y=(-1*radius);y<=(radius);y++){
                    iy=Math.abs(i+y);
                    if(iy>=height)
                        iy-=(2*(iy-(height-1)));
                    for(int x=(-1*radius);x<=radius;x++){
                        ix=Math.abs(j+x);
                        if(ix>=width)
                            ix-=(2*(ix-(width-1)));

                        grey=Color.red(pixelsbase[iy*width+ix]);
                        temp+=(Math.abs(grey*filter[(x+(radius))][(y+(radius))]));

                    }
                }
                temp/=8;
                temp+=255;
                temp/=2;
                temp/=255;
                pixels[i*width+j]=Color.rgb(temp,temp,temp);
            }
        }
        Bitmap result = bmp.copy(bmp.getConfig(), true);
        result.setPixels(pixels,0,width,0,0,width,height);
        return result;
    }

    /**
     *
     * @param vertical the Bitmap with the vertical edge detected
     * @param horizontal the Bitmap with the horizontal edge detected
     * @return a Bitmap with both edge detected
     */
    public static Bitmap SumSobel (Bitmap vertical,Bitmap horizontal){
        //j'ajoute bmp pour pouvoir différencier les fortes valeurs (blancs) présent dans l'original
        // des ptet pas ici finalement
        //pas fait pour sobel ou laplace
        int height = vertical.getHeight();
        int width = vertical.getWidth();
        int temph;
        int tempv;

        int pixels[] = new int[height*width];
        int verticalbase[] = new int[height*width];
        int horizontalbase[] = new int[height*width];
        float /*int*/ grey=0;
        int ix;
        int iy;
        float temp=0;
        float max = (2*(255*255));
        vertical.getPixels(verticalbase,0,width,0,0,width,height);
        horizontal.getPixels(horizontalbase,0,width,0,0,width,height);
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                temph=Color.red(horizontalbase[i*width+j]);
                tempv=Color.red(verticalbase[i*width+j]);
                //temp=(temph>tempv) ? temph : tempv;
                temp = (float)Math.sqrt((temph*temph + tempv*tempv));

                temp = (temp/max)*255;
                //La je seuille le par 230 mais ce serait une opération à ajouter après
                /*if(temp<230)
                    temp=0;*/
                pixels[i*width+j]=Color.rgb(temp,temp,temp);
            }

        }
        Bitmap result = vertical.copy(vertical.getConfig(), true);
        result.setPixels(pixels,0,width,0,0,width,height);
        return result;
    }


    /**
     * Adjust the brightness of the image according to the value
     * @param bmp a Bitmap on which we apply the function
     * @param value which is how much we change the brightness
     * @return the Bitmap modified
     */
    public static Bitmap Luminosity(Bitmap bmp, int value)
    {
        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);
        int totalSize = bmpResult.getWidth() * bmpResult.getHeight();
        int[] pixs = new int[totalSize];
        bmpResult.getPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(),bmpResult.getHeight());
        for(int i = 0; i < totalSize; i++)
        {
            int p = pixs[i];
            int red = Color.red(p) + value;
            int green = Color.green(p)+ value;
            int blue = Color.blue(p)+ value;
            int alpha = Color.alpha(p);
            if(red > 255)
                red = 255;
            if(blue > 255)
                blue = 255;
            if(green > 255)
                green = 255;
            if(red < 0)
                red = 0;
            if(blue < 0)
                blue = 0;
            if(green < 0)
                green = 0;
            pixs[i] = Color.argb(alpha,red,green,blue);
        }
        bmpResult.setPixels(pixs,0,bmpResult.getWidth(),0,0,bmpResult.getWidth(), bmpResult.getHeight());
        return bmpResult;
    }

    public static Bitmap Contrast (Bitmap bmp, double value){

        int width = bmp.getWidth();
        int height = bmp.getHeight();

        Bitmap bmpResult = bmp.copy(Bitmap.Config.ARGB_8888, true);

        int A, R, G, B;
        int pixel;

        // Valeur de contraste
        double contrast = Math.pow((100 + value) / 100, 2);

        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {

                pixel = bmp.getPixel(x, y);
                A = Color.alpha(pixel);

                //Application du contraste
                R = Color.red(pixel);
                R = (int)(((((R / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(R < 0) { R = 0; }
                else if(R > 255) { R = 255; }

                G = Color.green(pixel);
                G = (int)(((((G / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(G < 0) { G = 0; }
                else if(G > 255) { G = 255; }

                B = Color.blue(pixel);
                B = (int)(((((B / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
                if(B < 0) { B = 0; }
                else if(B > 255) { B = 255; }

                bmpResult.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        return bmpResult;
    }


    public static Bitmap Cartoon(Bitmap bmp){
        // les contours marchent plutot bien mais les blans sont pris en compte a cause de sobel
        int height = bmp.getHeight();
        int width = bmp.getWidth();
        Bitmap result;
        Bitmap sobel;
        int xi=0;
        int yi=0;
        int k;
        int l;
        int red;
        int green;
        int blue;
        //Bitmap tmp = bmp;
        int bmppixels[] = new int[height*width];
        bmp.getPixels(bmppixels,0,width,0,0,width,height);
        sobel = Convolution(bmp,3,"Sobel");//.copy(bmp.getConfig(),bmp.isMutable());
        int sobelpixels[] = new int[height*width];
       // bmp.getPixels(bmppixels,0,width,0,0,width,height);
        sobel.getPixels(sobelpixels,0,width,0,0,width,height);
        for(int i=0;i<height;i++){
            for(int j=0;j<width;j++){
                if(Color.red(sobelpixels[i*width+j]) > 145 /*&& Color.red(bmppixels[i*width+j]) < 230)*/){
                    for(k=0;k<3;k++)
                        yi=Math.abs(i+k);
                        if(yi>=height)
                            yi-=(2*(yi-(height-1)));
                        for(l=0;l<3;l++) {
                            xi=Math.abs(j+l);
                            if(xi>=width)
                                xi-=(2*(xi-(width-1)));
                        }
                            bmppixels[yi * width + xi] = Color.rgb(0, 0, 0);
                            //bmppixels[i * width + j] = Color.rgb(0, 0, 0);
                }
                else{
                red = Color.red(bmppixels[i*width+j]);
                green = Color.green(bmppixels[i*width+j]);
                blue = Color.blue(bmppixels[i*width+j]);
                red = red/80; green = green/80; blue = blue/80;
                red *= 80;green *= 80;blue *= 80;
                bmppixels[i * width + j] = Color.rgb(red,green,blue);}
                }
            }
        /*}
        /*if(iy>=height)
            iy-=(2*(iy-(height-1)));
        for(int x=(-1*radius);x<=radius;x++){
            ix=Math.abs(j+x);
            if(ix>=width)
                ix-=(2*(ix-(width-1)));

            grey=Color.red(pixelsbase[iy*width+ix]);
            temp+=(Math.abs(grey*filter[(x+(radius))][(y+(radius))]));*/
        result = bmp.copy(bmp.getConfig(),bmp.isMutable());
        result.setPixels(bmppixels,0,width,0,0,width,height);
        return result;
    }

}
