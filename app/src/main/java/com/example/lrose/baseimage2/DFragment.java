package com.example.lrose.baseimage2;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by lrose on 05/04/19.
 */

public class DFragment extends DialogFragment {
    private static final String TAG = "MyCustomDialog";

    public interface OnInputListener{
        void sendInput(String input);
    }
    public OnInputListener mOnInputListener;

    //widgets
    private EditText mInput;
    private TextView mActionOk, mActionCancel;

    //a ne pas utiliser
    String ss;


    //vars

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.dialogfragment, container, false);
        mActionCancel = view.findViewById(R.id.action_cancel);
        mActionOk = view.findViewById(R.id.action_ok);
        mInput = view.findViewById(R.id.input);

        mActionCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: closing dialog");
                getDialog().dismiss();
            }
        });


        mActionOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: capturing input");

                String input = mInput.getText().toString();
                int b = 0;
                if(!input.equals("")){
//
                    //Easiest way: just set the value
                    //((MainActivity)getActivity()).mInputDisplay.setText(input);
                    //((MainActivity)getActivity()).mInputDisplay.setText(input);
                    try {
                        int i = Integer.parseInt(input);
                        if(i%2==1) {
                            ((TextView) ((MainActivity) getActivity()).findViewById(R.id.mInputDisplay)).setText(input);
                            b = 1;
                        }
                        ((MainActivity) getActivity()).p=BitmapModifier.Convolution(((MainActivity) getActivity()).p,i,ss);
                        ImageView imgv = (ImageView) ((MainActivity) getActivity()).findViewById(R.id.imgv);
                        imgv.setImageBitmap(((MainActivity) getActivity()).p);
                    }
                    catch (NumberFormatException e){
                        mInput.setText("Entree Incorrecte");
                        b=0;
                    }
                    /*String s= ((ImageView)findViewById(R.id.mInputDisplay)).getText().toString();
                    int size = Integer.parseInt(s);
                    if(input%2 == 1)
                        p = BitmapModifier.Convolution(p,size,"Averaging");*/
                    //((TextView)view.findViewById(R.id.mInputDisplay)).setText("");
//
                }

                //"Best Practice" but it takes longer
                //mOnInputListener.sendInput(input);
                if(b==1)
                getDialog().dismiss();

            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try{
            mOnInputListener = (OnInputListener) getActivity();
        }catch (ClassCastException e){
            Log.e(TAG, "onAttach: ClassCastException: " + e.getMessage() );
        }
    }
}

