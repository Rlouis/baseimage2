package com.example.lrose.baseimage2;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;

import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
//import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v8.renderscript.Allocation;
import android.support.v8.renderscript.RenderScript;
import android.util.Log;
import android.view.View;
import android.webkit.JavascriptInterface;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v4.app.DialogFragment;


import com.android.rssample.ScriptC_grey;
import com.android.rssample.ScriptC_rcolor;

import java.io.IOException;
import java.util.Random;

public class MainActivity extends FragmentActivity implements DFragment.OnInputListener{

    protected Bitmap baseimage;
    protected Bitmap p;
    protected int value_seekbar;
    protected double value_seekBarContrast;
    protected int value_seekbarColor;
    protected int COLOR_CHOSEN = 0;
    int PICK_IMAGE_REQUEST=1;
    int NAME_REQUEST=2;

    private static final String TAG = "MainActivity";

    //widgets
    private Button mOpenDialog;
    public TextView mInputDisplay;

    //vars
    public String mInput;

    @Override
    public void sendInput(String input) {
        Log.d(TAG, "sendInput: got the input: " + input);

//        mInputDisplay.setText(input);
        mInput = input;

        setInputToTextView();
    }

    private void setInputToTextView(){
        mInputDisplay.setText(mInput);
    }
    FragmentManager fm = getSupportFragmentManager();
    /**
     *
     * @param bmp the Bitmap to modify
     */
    public void  toARandomColorRS(Bitmap bmp) {
        //Fonction que précedemment mais qui utilise Renderscript
        Random r = new Random();
        int color;
        int pixel;
        float[] hsv =new float[3];
        color=r.nextInt(360);

        //1)  Creer un  contexte  RenderScript
        RenderScript rs = RenderScript.create(this);
//2)  Creer  des  Allocations  pour  passer  les  données
        Allocation input = Allocation.createFromBitmap(rs , bmp);
        Allocation  output = Allocation.createTyped(rs , input.getType ());
//3)  Creer le  script
        ScriptC_rcolor rcolorScript = new  ScriptC_rcolor(rs);

        rcolorScript.set_color(color);
        rcolorScript.forEach_toRcolor(input, output);
        output.copyTo(bmp);
        input.destroy (); output.destroy ();
        rcolorScript.destroy (); rs.destroy ();
    }

    /**
     *
     * @param bmp the Bitmap to modify
     */
    public  void  toGreyRS(Bitmap  bmp) {
//1)  Creer un  contexte  RenderScript
        RenderScript rs = RenderScript.create(this);
//2)  Creer  des  Allocations  pour  passer  les  donnees
        Allocation input = Allocation.createFromBitmap(rs , bmp);
        Allocation  output = Allocation.createTyped(rs , input.getType ());
//3)  Creer le  script
        ScriptC_grey greyScript = new  ScriptC_grey(rs);

        greyScript.forEach_toGrey(input, output);
        output.copyTo(bmp);
        input.destroy (); output.destroy ();
        greyScript.destroy (); rs.destroy ();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ImageView imgv = (ImageView) findViewById(R.id.imgv);
        Resources res = getApplicationContext().getResources();
        BitmapFactory.Options options1 = new BitmapFactory.Options();
        options1.inJustDecodeBounds=true;
        BitmapFactory.Options options2 = new BitmapFactory.Options();
        options2.inMutable=true;
        //BitmapFactory.decodeResource(res,R.drawable.bbcv,options1);
        //baseimage = BitmapFactory.decodeResource(res,R.drawable.bbcv,options2);
        baseimage = BitmapFactory.decodeResource(res,R.drawable.etal,options2);
        //p=Bitmap.createBitmap(baseimage);
        p=baseimage.copy(baseimage.getConfig(),baseimage.isMutable());
        //p = BitmapFactory.decodeResource(res,R.drawable.etal,options2);
        //p=BitmapFactory.decodeResource(res,R.drawable.blanccarrenoir,options2);


        final Button gbutton = findViewById(R.id.buttonGray);
        gbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                toGreyRS(p);
                //p = BitmapModifier.toGray(p);
                //p = BitmapModifier.toGrayPixels(p);
                imgv.setImageBitmap(p);
            }
        });
        final Button buttonColor = findViewById(R.id.buttonColor);
        buttonColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                //toARandomColor(p);
                //toARandomColorRS(p);
                p = BitmapModifier.toColorize(p, COLOR_CHOSEN);
                imgv.setImageBitmap(p);
            }
        });
        final Button buttonColorConserv = findViewById(R.id.buttonColorConserv);
        buttonColorConserv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                //p = BitmapModifier.keepGreen(p);
                //imgv.setImageBitmap(p);
                //Intent intent = new Intent(getApplicationContext(), KeepActivity.class);
                //startActivityForResult(intent,1) ;
                p = BitmapModifier.toKeep(p, COLOR_CHOSEN, value_seekbarColor);
                imgv.setImageBitmap(p);
            }
        });
        final Button button4 = findViewById(R.id.buttonDynamic);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                p = BitmapModifier.dynamicExtensionOnGray(p);
                imgv.setImageBitmap(p);

            }
        });
        final Button button5 = findViewById(R.id.buttonEqualization);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                toGreyRS(p);
                //imgv.setImageBitmap(p);
                p = BitmapModifier.histogramEqualization(p);
                imgv.setImageBitmap(p);

            }
        });
        final Button button6 = findViewById(R.id.buttonConvolution);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                //toGreyRS(p);
                //imgv.setImageBitmap(p);
                //p = BitmapModifier.Convolution(p);
                //p = BitmapModifier.Convolution(p,3,"Sobel");
                //p = BitmapModifier.Convolution(p,3,"Sobel");
                p = BitmapModifier.Cartoon(p);
                imgv.setImageBitmap(p);

            }
        });
        final Button button13 = findViewById(R.id.buttonAveraging);
        button13.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                //toGreyRS(p);
                //imgv.setImageBitmap(p);
                //p = BitmapModifier.Convolution(p);
                //p = BitmapModifier.Convolution(p,3,"Sobel");
                //p = BitmapModifier.Convolution(p,3,"Sobel");
                /*p = BitmapModifier.Cartoon(p);
                imgv.setImageBitmap(p);*/
                /*AlertDFragment alertdFragment = new AlertDFragment();
                // Show Alert DialogFragment
                alertdFragment.show(fm, "Alert Dialog Fragment");*/
                Log.d(TAG, "onClick: opening dialog.");
                DFragment dialog = new DFragment();
                dialog.ss = "Averaging";
                dialog.show(fm, "Averaging");
                /*String s= ((TextView)findViewById(R.id.mInputDisplay)).getText().toString();
                int size = Integer.parseInt(s);

                if(size%2 == 1)
                    p = BitmapModifier.Convolution(p,size,"Averaging");
                imgv.setImageBitmap(p);
                //dialog.onActivityResult(0,0, );
                if(size%2 == 1)
                    p = BitmapModifier.Convolution(p,size,"Averaging");
                imgv.setImageBitmap(p);*/
            }
        });
        final Button button14 = findViewById(R.id.buttonGaussian);
        button14.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                //toGreyRS(p);
                //imgv.setImageBitmap(p);
                //p = BitmapModifier.Convolution(p);
                //p = BitmapModifier.Convolution(p,3,"Sobel");
                //p = BitmapModifier.Convolution(p,3,"Sobel");
                /*p = BitmapModifier.Cartoon(p);
                imgv.setImageBitmap(p);*/
                /*AlertDFragment alertdFragment = new AlertDFragment();
                // Show Alert DialogFragment
                alertdFragment.show(fm, "Alert Dialog Fragment");*/
                Log.d(TAG, "onClick: opening dialog.");
                //p = BitmapModifier.Convolution(p,3,"Gaussian");
                DFragment dialog = new DFragment();
                dialog.ss = "Gaussian";
                dialog.show(fm, "MyCustomDialog");



            }
        });
        final Button button11 = findViewById(R.id.buttonSobel);
        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                p = BitmapModifier.Convolution(p,3,"Sobel");
                imgv.setImageBitmap(p);

            }
        });
        final Button button12 = findViewById(R.id.buttonLaplacian);
        button12.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                //toGreyRS(p);
                //imgv.setImageBitmap(p);
                p = BitmapModifier.Convolution(p,3,"Laplacian");
                imgv.setImageBitmap(p);

            }
        });

        final Button button7 = findViewById(R.id.buttonReinitialisation);
        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                BitmapFactory.Options options2 = new BitmapFactory.Options();
                options2.inMutable=true;
                //Resources res = getApplicationContext().getResources();
                //p=BitmapFactory.decodeResource(res,R.drawable.blanccarrenoir,options2);
                //p=BitmapFactory.decodeResource(res,R.drawable.etal,options2);
                //p=Bitmap.createBitmap(baseimage);
                p=baseimage.copy(baseimage.getConfig(),baseimage.isMutable());
                imgv.setImageBitmap(p);
            }
        });
        final Button button8 = findViewById(R.id.buttonImport);
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("image/*");
                startActivityForResult(intent, 1);
            }
        });

        final Button button10 = findViewById(R.id.buttonSave);
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MediaStore.Images.Media.insertImage(getContentResolver(), p, "test" , "");

            }
        });

        final Button button9 = findViewById(R.id.buttonLuminosity);
        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                p = BitmapModifier.Luminosity(p, value_seekbar);
                imgv.setImageBitmap(p);
            }
        });

        final SeekBar seekbar = findViewById(R.id.seekbar);
        final TextView textseekbar = findViewById(R.id.textseekbar);
        seekbar.setMax(510);
        seekbar.setProgress(255);
        value_seekbar = -255 + seekbar.getProgress();
        textseekbar.setText("Luminosité : " +value_seekbar);
        seekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value_seekbar = -255 + seekbar.getProgress();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                textseekbar.setText("Luminosité : " +value_seekbar);
            }
        });

        final Button buttonContrast = findViewById(R.id.buttonContrast);
        buttonContrast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ImageView imgv = (ImageView) findViewById(R.id.imgv);
                p = BitmapModifier.Contrast(p, value_seekBarContrast);
                imgv.setImageBitmap(p);
            }
        });

        final SeekBar seekBarContrast = findViewById(R.id.seekBarContrast);
        final TextView textseekbar2 = findViewById(R.id.text_seekBarContrast);
        seekBarContrast.setMax(100);
        seekBarContrast.setProgress(0);
        value_seekBarContrast = seekBarContrast.getProgress();
        textseekbar2.setText("Contraste : " +value_seekBarContrast);
        seekBarContrast.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value_seekBarContrast = seekBarContrast.getProgress();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                textseekbar2.setText("Contraste : " +value_seekBarContrast);
            }
        });


        final Button b_red = findViewById(R.id.b_red);
        b_red.setBackgroundColor(Color.RED);
        b_red.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            COLOR_CHOSEN = 0;
            }
        });
        final Button b_blue = findViewById(R.id.b_blue);
        b_blue.setBackgroundColor(Color.BLUE);
        b_blue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                COLOR_CHOSEN = 1;
            }
        });
        final Button b_green = findViewById(R.id.b_green);
        b_green.setBackgroundColor(Color.GREEN);
        b_green.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                COLOR_CHOSEN = 2;
            }
        });
        final Button b_magenta = findViewById(R.id.b_magenta);
        b_magenta.setBackgroundColor(Color.MAGENTA);
        b_magenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                COLOR_CHOSEN = 3;
            }
        });
        final Button b_yellow = findViewById(R.id.b_yellow);
        b_yellow.setBackgroundColor(Color.YELLOW);
        b_yellow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                COLOR_CHOSEN = 4;
            }
        });
        final Button b_cyan = findViewById(R.id.b_cyan);
        b_cyan.setBackgroundColor(Color.CYAN);
        b_cyan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                COLOR_CHOSEN = 5;
            }
        });

        final SeekBar seekbarColor = findViewById(R.id.seekbarColor);
        final TextView textseekbar3 = findViewById(R.id.text_seekbarColor);
        seekbarColor.setMax(255);
        seekbarColor.setProgress(0);
        value_seekbarColor = seekbarColor.getProgress();
        textseekbar3.setText("Keep:        " +value_seekbarColor);
        seekbarColor.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value_seekbarColor = seekbarColor.getProgress();
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                textseekbar3.setText("Keep:        " +value_seekbarColor);
            }
        });


        int bitmapHeight =p.getHeight();
        int bitmapWidth = p.getWidth();
        imgv.setImageBitmap(p);

    }


    @Override
    protected void onActivityResult(int requestCode,int resultCode,Intent data){
        super.onActivityResult(requestCode,resultCode,data);
        ImageView imgv = (ImageView) findViewById(R.id.imgv);
        Uri filePath;
        if(requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            filePath = data.getData();

            try{
                baseimage = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath).copy(baseimage.getConfig(),baseimage.isMutable());
                //p=baseimage;
                p=baseimage.copy(baseimage.getConfig(),baseimage.isMutable());
                imgv.setImageBitmap(p);
            }catch (IOException e){
                e.printStackTrace();
            }
        }
        if(requestCode == NAME_REQUEST && resultCode == RESULT_OK){
            String result=data.getStringExtra("result");
            MediaStore.Images.Media.insertImage(getContentResolver(), p, result , "bold");


        }
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
    }


}
