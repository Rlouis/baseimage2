#pragma  version (1)
#pragma  rs  java_package_name(com.android.rssample)
int color=0;
uchar4  RS_KERNEL  toRcolor(uchar4  in) {
float4  pixelf = rsUnpackColor8888(in);
float  cmax;
float rprime = pixelf.r/255;
float gprime = pixelf.g/255;
float bprime = pixelf.b/255;
cmax = fmax(fmax(rprime, bprime),gprime);
float cmin =fmin(fmin(rprime, bprime),gprime);
float delta=cmax-cmin;
float hue;
float saturation;
float value;
if(delta == 0){
hue=0;
}
else
if(cmax==rprime){
    hue=60*(fmod(((gprime-bprime)/delta),6));
}
else
if(cmax==gprime){
    hue=60*(((bprime-rprime)/delta)+2);
}
else
if(cmax==bprime){
    hue=60*(((rprime-gprime)/delta)+4);
}
if(cmax==0){
    saturation = 0;
}
else
saturation=delta/cmax;
value=cmax;
hue=color;

float c=value*saturation;
float x=c*(1-(fabs(fmod((hue/60),2))-1));
float m=value-c;

if(hue<360){
    if(hue<300){
        if(hue<240){
            if(hue<180){
                if(hue<120){
                    if(hue<60){
                        rprime=c;
                        gprime=x;
                        bprime=0;
                    }
                    else{
                        rprime=x;
                        gprime=c;
                        bprime=0;
                    }
                }
                else{
                    rprime=0;
                    gprime=c;
                    bprime=x;
                }

            }
            else{
                rprime=0;
                gprime=x;
                bprime=c;
            }

        }
        else{
            rprime=x;
            gprime=0;
            bprime=c;
            }
    }
    else{
        rprime=c;
        gprime=0;
        bprime=x;
        }
}
float red = (rprime+m)*255;
float green = (gprime+m)*255;
float blue = (bprime+m)*255;
return  rsPackColorTo8888(red , green , blue , pixelf.a);
}